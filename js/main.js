   	// inner variables
    var song;
	var check = false
	var tracker = '';
    var volume = '';
	
    function initAudio(elem) {
        var url = elem.attr('audiourl');
        var title = elem.text();
        var cover = elem.attr('cover');
        var artist = elem.attr('artist');
		
		$('.testlist li').each(function(i, obj){
			if($(obj).attr('id') == title){
				check = true;
			}
		});
				
		if(check == false){
			return;
		}

        $('.player .title').text(title);
        $('.player .artist').text(artist);
        $('.player .cover').css('background-image','url(data/' + cover+')');;

        song = new Audio('data/' + url);

        // timeupdate event listener
        song.addEventListener('timeupdate',function (){
            var curtime = parseInt(song.currentTime, 10);
            tracker.slider('value', curtime);
        });

        $('.playlist li').removeClass('active');
		$('.testlist li').removeClass('active');
        elem.addClass('active');
		
		$('.testlist li').each(function(i, obj){
			if($(obj).attr('id') == title){
				$(obj).addClass('active');
			}
		});
		check = false;
		setVolume();
    }
    function playAudio() {
        song.play();

        tracker.slider("option", "max", song.duration);

        $('.play').addClass('hidden');
        $('.pause').addClass('visible');
    }
    function stopAudio() {
        song.pause();

        $('.play').removeClass('hidden');
        $('.pause').removeClass('visible');
    }

    // initialization - first element in playlist
	function run(){
		$('.playlist li').each(function(i, obj){
			initAudio($(obj));
			if(check == true){
				return false;
			}
		});
	}
			
    // set volume
	function setVolume(){
		song.volume = 0.8;
	}
	
jQuery(document).ready(function() {

	tracker = $('.tracker');
	volume = $('.volume');

	// play click
    $('.play').click(function (e) {
        e.preventDefault();

        playAudio();
    });

    // pause click
    $('.pause').click(function (e) {
        e.preventDefault();

        stopAudio();
    });

    // forward click
    $('.fwd').click(function (e) {
        e.preventDefault();

        stopAudio();
        var next = $('.testlist li.active').next();
		$('.playlist li').each(function(i, obj){
			if($(obj).text() == next.text()){
				initAudio($(obj));
			}
		});
    });

    // rewind click
    $('.rew').click(function (e) {
        e.preventDefault();

        stopAudio();

        var prev = $('.testlist li.active').prev();
		$('.playlist li').each(function(i, obj){
			if($(obj).text() == prev.text()){
				initAudio($(obj));
			}
		});
    });
	
	// initialize the volume slider
	volume.slider({
		range: 'min',
		min: 1,
		max: 100,
		value: 80,
		start: function(event,ui) {},
		slide: function(event, ui) {
			song.volume = ui.value / 100;
		},
		stop: function(event,ui) {},
	});
	
	
    // empty tracker slider
	tracker.slider({
		range: 'min',
		min: 0, max: 10,
		start: function(event,ui) {},
		slide: function(event, ui) {
			song.currentTime = ui.value;
		},
		stop: function(event,ui) {}
	});
});